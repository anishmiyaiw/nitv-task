from django.urls import path

from . import views
from .views import IndexView

urlpatterns = [
    path("", IndexView.as_view(), name="home"),
    path("profile-list/", views.profile_list, name="profile-list"),
    path("profile-add/", views.profile_add, name="profile-add"),
    path('person_details_view/<int:pk>', views.person_details_view, name='person_details_view'),
    path('search-person/', views.search_person, name='search-person'),

]
