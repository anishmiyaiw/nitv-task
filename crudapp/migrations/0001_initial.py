# Generated by Django 4.2.1 on 2023-05-26 05:19

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20)),
                ('profile_image', models.ImageField(default='profile_image/avatar.png', upload_to='profile_image/')),
                ('phone', models.IntegerField(unique=True)),
                ('email', models.EmailField(max_length=254)),
                ('nationality', models.CharField(max_length=20)),
                ('dob', models.DateTimeField()),
                ('gender', models.CharField(choices=[('M', 'Male'), ('F', 'Female'), ('N', 'None')], default='N', max_length=1)),
                ('mode_of_contact', models.CharField(choices=[('PH', 'Phone'), ('EM', 'Email'), ('NO', 'None')], default='N', max_length=2)),
            ],
        ),
        migrations.CreateModel(
            name='Academic',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('degree', models.CharField(max_length=20)),
                ('program', models.CharField(max_length=20)),
                ('college_name', models.CharField(max_length=30)),
                ('graduation_year', models.DateField()),
                ('profile', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='crudapp.profile')),
            ],
        ),
    ]
