from django.db import models

# Create your models here.

from django.db import models
from utils import validator

PHONE = "PH"
EMAIL = "EM"
NONE = "NO"

MODE_OF_CONTACT_CHOICES = [
    (PHONE, "Phone"),
    (EMAIL, "Email"),
    (NONE, "None")
]

MALE = "M"
FEMALE = "F"
NONE = "N"  # not specified

GENDER = [
    (MALE, "Male"),
    (FEMALE, "Female"),
    (NONE, "None")
]


# Create your models here.
class Profile(models.Model):
    name = models.CharField(max_length=20)
    profile_image = models.ImageField(upload_to='profile_image/', default="profile_image/avatar.png", blank=True,
                                      null=True)
    phone = models.IntegerField(unique=True)
    email = models.EmailField()
    nationality = models.CharField(max_length=20)
    dob = models.DateTimeField()
    gender = models.CharField(max_length=1, choices=GENDER, default=NONE)
    mode_of_contact = models.CharField(max_length=2, choices=MODE_OF_CONTACT_CHOICES, default=NONE)

    def __str__(self):
        return self.name


class Academic(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    degree = models.CharField(max_length=20)
    program = models.CharField(max_length=20)
    college_name = models.CharField(max_length=30)
    graduation_year = models.DateField()

    def __str__(self):
        return self.profile.name
