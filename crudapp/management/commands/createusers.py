from django.core.management.base import BaseCommand
from django.contrib.auth.models import User


class Command(BaseCommand):
    help = 'Creates sample users.'

    def handle(self, *args, **options):
        # Create and save users
        user = User.objects.create_user(username='admin', password='admin')
        user1 = User.objects.create_user(username='user1', password='password1')
        user2 = User.objects.create_user(username='user2', password='password2')

        user.email = 'admin@example.com'
        user.first_name = 'ram'
        user.last_name = 'gurung'
        user.save()

        user1.email = 'user1@example.com'
        user1.first_name = 'hari'
        user1.last_name = 'kaka'
        user1.save()

        user2.email = 'user2@example.com'
        user2.first_name = 'sita'
        user2.last_name = 'mata'
        user2.save()

        self.stdout.write(self.style.SUCCESS('Users created successfully.'))
