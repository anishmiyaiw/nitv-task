from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator

from django.views.generic import TemplateView
from django.template import loader
from django.http import HttpResponse
from django.shortcuts import redirect

from crudapp.models import Academic, Profile


class IndexView(LoginRequiredMixin, TemplateView):
    template_name = "index.html"


@login_required
def profile_list(request):
    profile_list = Profile.objects.all()
    paginator = Paginator(profile_list, 25)  # Show 25 contacts per page.

    page_number = request.GET.get("page")
    page_obj = paginator.get_page(page_number)

    template = loader.get_template("index.html")
    context = {
        "profile_list": profile_list,
        "page_obj": page_obj
    }
    return HttpResponse(template.render(context, request))


@login_required
def profile_add(request):
    template = loader.get_template("profile_add.html")
    context = {}
    if request.method == "POST":
        try:
            post_data = request.POST
            if request.FILES.get('image') is None:
                profile_image = 'profile_image/avatar.png'
            else:
                profile_image = request.FILES.get('image')

            save_profile = Profile.objects.create(name=post_data.get('name'), phone=post_data.get("phone"),
                                                  nationality=post_data.get('nationality'),
                                                  dob=post_data.get('dob'),
                                                  mode_of_contact=post_data.get("mode_of_contact"),
                                                  email=post_data.get("email"),
                                                  gender=post_data.get("gender"),
                                                  profile_image=profile_image)

            degree = request.POST.getlist("degree")
            if degree:
                program = request.POST.getlist("program")
                college_name = request.POST.getlist("college_name")
                g_year = request.POST.getlist("g_year")
                for x in range(0, int(program.__len__())):
                    Academic.objects.create(profile=save_profile, degree=degree[x], program=program[x],
                                            college_name=college_name[x], graduation_year=g_year[x])
            return redirect('/profile-list/')
        except:
            pass
    return HttpResponse(template.render(context, request))


@login_required
def profile_list(request):
    profile_list = Profile.objects.all()
    template = loader.get_template("index.html")
    context = {
        "profile_list": profile_list,
    }
    return HttpResponse(template.render(context, request))


@login_required
def person_details_view(request, pk):
    template = loader.get_template("profile_details.html")
    profile = Profile.objects.get(pk=pk)
    academics = Academic.objects.filter(profile=pk)
    context = {
        "profile": profile,
        "academics": academics

    }

    return HttpResponse(template.render(context, request))


def search_person(request):
    profile_list = Profile.objects.all()
    if request.method == "POST":
        try:
            profile_list = Profile.objects.filter(name__contains=request.POST.get('search'))

        except:
            profile_list = Profile.objects.all()
    paginator = Paginator(profile_list, 25)  # Show 25 contacts per page.

    page_number = request.GET.get("page")
    page_obj = paginator.get_page(page_number)

    template = loader.get_template("index.html")
    context = {
        "profile_list": profile_list,
        "page_obj": page_obj
    }

    return HttpResponse(template.render(context, request))
