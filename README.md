# Django CRUD Task

This version uses Django 4.2.1. version.

## Installation and Running

Description of the installation steps:

    # Clone the project
    git clone git@github.com:rayed/django-crud-parent-child.git

    cd crudtask
    
    # Create Python 3 virtual environment 
    python3 -m venv venv

    # Activate the virtual environment
    source venv/bin/activate

    # Install required packages
    pip install -r requirements.txt

    # Create database tables for the project, project configured to use SQLite DB
    python manage.py migrate

    # (Insert users directly into the database)

    python manage.py createusers


    # Run the development server
    python manage.py runserver

    # Goto Web application
    http://127.0.0.1:8000
    login 

    username= admin
    password= admin

